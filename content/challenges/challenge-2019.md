title: Challenge 2019 : navigation contre la montre
menulabel: Challenge 2019
status: published


### Résultats de la course du 27 novembre 2019

#### Classement général au temps

| Rang | Équipe                              | Temps total |
| ---- | ----------------------------------- | ---------   |
|  1   | Maxime LAMBERT & Léo ROUZIC         |        35 s |
|  2   | François AUXIETRE & Adrien WEYL     |        37 s |
|  3   | Valentin DUVAL & Guillaume HEMBERT  |        38 s |
|  4   | Faustine CHARRETTE & Paul MONTI     |        44 s |
|  5   | Pierre-Alexis ODYE                  |  1 min 19 s |
|  -   | Mamadou COULIBALY & Valentin UTIEL  |  DNF        |

#### Classement au temps sur route

| Rang | Équipe                              | Temps       |
| ---- | ----------------------------------- | ---------   |
|  1   | Maxime LAMBERT & Léo ROUZIC         |        29 s |
|  2   | François AUXIETRE & Adrien WEYL     |        30 s |
|  2   | Valentin DUVAL & Guillaume HEMBERT  |        30 s |
|  4   | Faustine CHARRETTE & Paul MONTI     |        35 s |
|  5   | Pierre-Alexis ODYE                  |  1 min 02 s |
|  6   | Mamadou COULIBALY & Valentin UTIEL  |  1 min 15 s |

#### Classement au temps les obstacles

| Rang | Équipe                              | Temps |
| ---- | ----------------------------------- | ----- |
|  1   | Maxime LAMBERT & Léo ROUZIC         |   6 s |
|  2   | François AUXIETRE & Adrien WEYL     |   7 s |
|  3   | Valentin DUVAL & Guillaume HEMBERT  |   8 s |
|  4   | Faustine CHARRETTE & Paul MONTI     |   9 s |
|  5   | Pierre-Alexis ODYE                  |  17 s |
|  -   | Mamadou COULIBALY & Valentin UTIEL  |  DNF  |

#### Prix spécial de la cartographie

Un prix spécial de la cartographie est attribué à François AUXIETRE et Adrien WEYL.


<!-- Applique le bon style sur les tableaux -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script language="javascript">$("table").addClass("table table-hover");</script>
