title: Challenge 2018 : navigation contre la montre
menulabel: Challenge 2018
status: published


### Résultats de la course du 30 novembre 2018

#### Classement général au temps

| Rang | Équipe                              | Temps total |
| ---- | ----------------------------------- | -------   |
|  1   | Dorian Screm & Kévin Paratre-Badois |      40 s |
|  2   | Quentin Angeli & Damien Nguyen      | 1 min 3 s |
|  3   | Pierre Miola & Mohamed Sefacene     | 1 min 6 s |
|  4   | Laurent Pierre                      | 1 min 16 s|
|  5   | Julien Garnier & Émilien Petit      | 1 min 22 s|
|  6   | Angélique Montuwy                   | 2 min 12 s|
|  7   | Yannick Namour & Mamady Conde       | 3 min 3 s |
|  -   | Mikael Le & Killian Rousseau        | DNF       |
|  -   | Prune Forget & Anthony Mirabile     | DNF       |

#### Classement au temps sur route

| Rang | Équipe                              |   Temps   |
| ---- | ----------------------------------- |   ----    |
|  1   | Dorian Screm & Kévin Paratre-Badois |      34 s |
|  2   | Pierre Miola & Mohamed Sefacene     |      51 s |
|  3   | Quentin Angeli & Damien Nguyen      |      53 s |
|  4   | Mikael Le & Killian Rousseau        |      58 s |
|  5   | Laurent Pierre                      |      59 s |
|  6   | Julien Garnier & Émilien Petit      | 1 min 3 s |
|  7   | Angélique Montuwy                   | 1 min 48 s|
|  8   | Yannick Namour & Mamady Conde       | 2 min 29 s|
|  -   | Prune Forget & Anthony Mirabile     | DNF       |

#### Classement au temps les obstacles

| Rang | Équipe                              | Temps |
| ---- | ----------------------------------- |  ---- |
|  1   | Dorian Screm & Kévin Paratre-Badois |   6 s |
|  2   | Quentin Angeli & Damien Nguyen      |  10 s |
|  3   | Pierre Miola & Mohamed Sefacene     |  15 s |
|  4   | Laurent Pierre                      |  17 s |
|  5   | Julien Garnier & Émilien Petit      | 19 s (dont 5 s pénalité collision) |
|  6   | Angélique Montuwy                   |  24 s |
|  7   | Yannick Namour & Mamady Conde       |  34 s |
|  -   | Mikael Le & Killian Rousseau        |  DNF  |
|  -   | Prune Forget & Anthony Mirabile     |  DNF  |

#### Prix spécial de la cartographie

Un prix spécial de la cartographie est attribué à Prune Forget et Anthony Mirabile.


<!-- Applique le bon style sur les tableaux -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script language="javascript">$("table").addClass("table table-hover");</script>
