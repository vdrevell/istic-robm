title: Documents pour l'option ROBM (M2 GL/CCN à l'ISTIC)
save_as: index.html
url: 
Menulabel: Home

Ce site héberge quelques notebooks interactifs pour l'option ROBM des Master 2 IL et CCN de l'ISTIC.
Ces derniers sont exécutables interactivement d'un simple clic grâce au service mybinder.org.
[Voir les notebooks]({category}notebooks).

Vous pouvez aussi accéder à [une section de vidéos choisies]({category}videos).

### Transparents de cours et TP
Les notes de cours et les sujets de TP sont disponibles sur 
[le serveur de fichiers de l'ISTIC](https://share-etu.istic.univ-rennes1.fr/m2info/ROBM/)
