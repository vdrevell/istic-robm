import numpy as np
from copy import copy, deepcopy

from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
output_notebook()

arrow_length = 0.1

def _scaled(w, inf=0.0, sup=1.0):
    w_min = np.min(w)
    w_max = np.max(w)
    eps = 0.000001
    input_range = w_max - w_min + eps
    output_range = sup - inf
    return inf + (w - w_min + eps) * output_range / input_range

def _scaled_w(particles, inf=0.0, sup=1.0):
    w = np.array([p.w for p in particles])
    return _scaled(w, inf, sup)

def init_display(particles, env_segments):
    global fig, r, r1, r2, arrow_length, r_estim, r_estim_dir
    fig = figure(title="Nuage de particules", plot_height=400, plot_width=400, x_range=(-2,3), y_range=(-2,3))
    for seg in env_segments:
        fig.line(seg[0], seg[1])
    p_x = [p.x for p in particles]
    p_y = [p.y for p in particles]
    p_dx= [[p.x, p.x+arrow_length*np.cos(p.theta)] for p in particles]
    p_dy= [[p.y, p.y+arrow_length*np.sin(p.theta)] for p in particles]
    p_w = np.array([p.w for p in particles])
    alpha = _scaled(p_w, 0.1, 1.0)
    r = fig.scatter(p_x, p_y, color="#2222aa", fill_alpha=alpha, line_color=None, size=_scaled(p_w, 1.0, 5.0))
    r1 = fig.line([], [], color="red", line_width=1)
    r2 = fig.multi_line(p_dx, p_dy, color="#2222aa", line_alpha=alpha)
    r_estim = fig.scatter([np.nan], [np.nan], color="#22bb22", line_color=None)
    r_estim_dir = fig.line([np.nan, np.nan], [np.nan, np.nan], color="#22bb22", line_width=1)
    show(fig, notebook_handle=True)

def update_display(particles, true_robot, estim=None):
    global fig, r, r1, r2, arrow_length, r_estim, r_estim_dir
    p_w = np.array([p.w for p in particles])
    alpha = _scaled(p_w, 0.1, 1.0)
    r.data_source.data['fill_alpha'] = alpha
    r.data_source.data['x'] = [p.x for p in particles]
    r.data_source.data['y'] = [p.y for p in particles]
    r.data_source.data['size'] = _scaled(p_w, 1.0, 5.0)
    #fig.renderers.remove(r)
    #r = fig.scatter([p.x for p in particles], [p.y for p in particles], color="#2222aa", 
    #      fill_alpha=[w[p]*len(particles) for p in particles], line_color=None)
    
    r2.data_source.data['xs'] =  [[p.x, p.x+arrow_length*np.cos(p.theta)] for p in particles]
    r2.data_source.data['ys'] =  [[p.y, p.y+arrow_length*np.sin(p.theta)] for p in particles]
    r2.data_source.data['line_alpha'] = alpha
    
    new_data = dict( x=[true_robot.x], y=[true_robot.y] )
    r1.data_source.stream( new_data, 300 )
    
    if estim != None:
        r_estim.data_source.data['x'] = [estim.x]
        r_estim.data_source.data['y'] = [estim.y]
        r_estim_dir.data_source.data['x'] =  [estim.x, estim.x+arrow_length*np.cos(estim.theta)]
        r_estim_dir.data_source.data['y'] =  [estim.y, estim.y+arrow_length*np.sin(estim.theta)]
    
    push_notebook()

def kitagawa_resample( particles ):
    #Rééchantillonnage d'un jeu de particules selon la mÈthode de Kitagawa
    #parts = list( zip( w, states ) )
    #parts = sorted(parts, key=lambda p: p[0])
    parts = sorted( particles, key=lambda p: p.w )
    
    N = len(parts)
    q = np.cumsum( [p.w for p in parts] ) #[p[0] for p in parts] )
    q /= q[-1] # normalize sum of weights
        
    sq = len(q);
    aux= np.random.rand(1);
    u= np.array( np.arange(aux, N+aux) / N );
    j=0;
    N_babies = [0]*N;
    for i in range(N):
        while (j<sq-1 and u[i]>q[j]):
            j += 1
        N_babies[j] += 1
    
    resampled = []
    
    for i in range(N):
        if (N_babies[i]>0):
            resampled.append( parts[i] )
            for j in range(1, N_babies[i]):
                new_part = deepcopy( parts[i] )
                resampled.append( new_part )
    
    #resampled_w, resampled_states = (list(t) for t in zip(*resampled_parts))

    sw = sum( [p.w for p in resampled] );
    for p in resampled:
        p.w /= sw
    
    return resampled

