import numpy as np
from numpy import cos, sin, pi, sqrt, array
from numba import njit, jit


class RobotSim:
    env_segments = np.array([[]])
    
    def process(self):
        for k in range(10):
            self.tick()
    
    def __init__(self):        
        self.t = 0.0

        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0

        self.v = 1.0 #*0
        self.w = 0.4 #*2

        self._last_control = -1e10
        self._control_avoid = False
        
        self._last_x = self.x;
        self._last_y = self.y;
        self._last_theta = self.theta;
        self._last_t = self.t;
    
    def measured_displacement(self):
        dx = self.x - self._last_x;
        dy = self.y - self._last_y;
        dtheta = self.theta - self._last_theta;
        dt = self.t - self._last_t;
        
        dx_bf = cos(-self._last_theta)*dx - sin(-self._last_theta)*dy
        dy_bf = sin(-self._last_theta)*dx + cos(-self._last_theta)*dy
        
        self._last_x = self.x;
        self._last_y = self.y;
        self._last_theta = self.theta;
        self._last_t = self.t;
        
        return dx_bf, dy_bf, dtheta, dt
    
    def command(self, v, w):
        self.v = v
        self.w = w
    
    def evolve(self, dt = 0.02):
        self.x += dt * self.v * cos(self.theta)
        self.y += dt * self.v * sin(self.theta)
        self.theta += dt * self.w
        self.t += dt
    
    def observe(self):
        return distance_cone_environnement(self.x, self.y, self.theta, self.env_segments, 10.0 * pi / 180.) \
            + np.random.uniform(-0.01, 0.01) + np.random.normal(0, 0.05)
    
    def control(self):
        #if (self.v>0):
        #    d = array([ cos(self.theta), sin(self.theta) ])
        #else:
        #    d = array([ -cos(self.theta), -sin(self.theta) ])
        #y = env_directional_distance(array([self.x, self.y]), d, self.env_segments)
        
        y = self.observe()
        
        #self.v = 1.0
        #if y > 3:
        #    self.w = 1.0
        #elif y < 2.7:
        #    self.w = 0.0
        if y < 0.30:
            self.v = -0.1 #self.v
            self.w = 4.0
            self._control_avoid = True
        elif self._control_avoid:
            self._control_avoid = False
            self.v = 1.0
            self.w = np.random.uniform(0.1,1.3)
        elif np.random.rand() < 0.1:
            self.w = np.random.uniform(-0.7,1.3)
    
    def tick(self, T_simu = 0.02, T_control = 0.1):
        if self.t > self._last_control + T_control:
            self._last_control = self.t
            self.control()
        self.evolve(T_simu)
    
    def state(self):
        return [self.x, self.y, self.theta]
    
    def plot_state(self, plt, color='r', arrow_len=0.15):
        plt.plot(self.x, self.y, color=color, marker='o')
        plt.plot([self.x, self.x + arrow_len * cos(self.theta)], [self.y, self.y + arrow_len * sin(self.theta)], color=color)


@njit
def segment_directional_distance(o, v, a, b):
    v1 = o - a
    v2 = b - a
    v3 = np.array([-v[1], v[0]], dtype=np.float64)
    v2_v3 = v2 @ v3
    if v2_v3 == 0: return np.inf
    t2 = (v1 @ v3) / (v2_v3)
    if not 0 <= t2 <= 1: return np.inf
    v2_X_v1 = v2[0]*v1[1] - v2[1]*v1[0]
    t1 = v2_X_v1 / v2_v3
    if t1 < 0: return np.inf
    return t1

@njit
def segment_directional_distance_no_cull(o, v, a, b):
    v1 = o - a
    v2 = b - a
    v3 = np.array([-v[1], v[0]], dtype=np.float64)
    v2_v3 = v2 @ v3
    if v2_v3 == 0: return (np.inf,np.inf)
    t2 = (v1 @ v3) / (v2_v3)
    v2_X_v1 = v2[0]*v1[1] - v2[1]*v1[0]
    t1 = v2_X_v1 / v2_v3
    return (t1, t2)

@njit
def segment_pie_distance(o, v1, v2, a, b):
    """ Return the shortest directionnal distance from o to line segment [ab] 
    within the cone defined by unit direction vectors v1 and v2
    """
    d1, s1 = segment_directional_distance_no_cull(o, v1, a, b)
    d2, s2 = segment_directional_distance_no_cull(o, v2, a, b)
    
    # Reject if both pie limit vectors point away from the supporting line (ab)
    if d1 < 0 and d2 < 0:
        return (np.inf,np.inf)
    
    if d1 < 0:
        # swap v1 and v2, to reduce to the d2<0 case
        v1,v2 = v2,v1
        (d1,s1),(d2,s2) = (d2,s2),(d1,s1)
    
    if d2 < 0:
        # If (d1,d2) angle spans in a->b direction
        if s2 < s1:
            if s1 > 1: # Reject if the cone only spans on the b-side invalid half-line
                return (np.inf,np.inf)
            else: # Cap to the b bound
                s2 = 1
                d2 = np.linalg.norm(b-o)
        else:
            if s1 < 0: # Reject if the cone only spans on the a-side invalid half-line
                return (np.inf,np.inf)
            else: # Cap to the a bound
                s2 = 0
                d2 = np.linalg.norm(a-o)
    
    # reorder v1 and v2
    if s1>s2:
        v1,v2 = v2,v1
        (d1,s1),(d2,s2) = (d2,s2),(d1,s1)
    
    # Reject if both vectors intersect on the same invalid half-line
    if s2 < 0 or s1 > 1:
        return (np.inf,np.inf)
    
    # Cap to the line-segment bounds
    if s1 < 0:
        s1 = 0
        d1 = np.linalg.norm(a-o)
    if s2 > 1:
        s2 = 1
        d2 = np.linalg.norm(b-o)
        
    # Compute orthogonal projection and distance
    ab = b-a
    n = np.array([-ab[1], ab[0]]) / np.linalg.norm(ab)
    dn, sn = segment_directional_distance_no_cull(o, n, a, b)
    
    # Return the orthogonal distance if the orthogonal projection is within the s1,s2 bounds
    if s1 < sn < s2:
        return ( np.abs(dn), sn )
    
    # Otherwise, min distance is on the bounds
    return (d1,s1) if d1<d2 else (d2,s2)


@jit
def env_directional_distance(o, v, env_segments):
    d = np.inf
    for seg in env_segments:
        a = np.array([seg[0][0], seg[1][0]], dtype=np.float64)
        b = np.array([seg[0][1], seg[1][1]], dtype=np.float64)
        ds = segment_directional_distance(o,v, a,b )
        if d>ds: d=ds
    return d


@jit
def env_pie_distance(o, v1, v2, env_segments):
    d = np.inf
    for seg in env_segments:
        a = np.array([seg[0][0], seg[1][0]], dtype=np.float64)
        b = np.array([seg[0][1], seg[1][1]], dtype=np.float64)
        ds = segment_pie_distance(o,v1,v2, a,b )[0]
        if d>ds: d=ds
    return d


@jit
def distance_cone_environnement(x, y, theta, env_segments, aperture=10.0 * pi / 180.):
    v1 = array([ cos(theta + aperture), sin(theta + aperture) ], dtype=np.float64)
    v2 = array([ cos(theta - aperture), sin(theta - aperture) ], dtype=np.float64)
    o = array([x, y], dtype=np.float64)
    return env_pie_distance(o, v1, v2, env_segments)


@jit
def distance_rayon_environnement(x, y, theta, env_segments):
    v = array([ cos(theta), sin(theta) ], dtype=np.float64)
    o = array([x, y], dtype=np.float64)
    return env_directional_distance(o, v, env_segments)
