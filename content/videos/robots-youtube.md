title: Quelques robots
summary: Quelques vidéos de robots...

### Boston Dynamics
#### Atlas
{% youtube LikxFZZO2sk %}

#### Spot-mini
{% youtube aFuA50H9uek %}


### Voilier autonome Vaimos
Ifremer et ENSTA Bretagne (Brest), vidéo de 2013.

{% youtube PJWlKkKgAsc %}


