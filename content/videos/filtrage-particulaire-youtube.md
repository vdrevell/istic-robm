title: Le filtrage particulaire
summary: Présentation en vidéo et sans équations (mais en anglais) du filtrage particulaire (*A. Svenson, Université d'Uppsala*).

Les filtres particulaires sont des méthodes de Monte-Carlo séquentielles pour l'estimation. 
Ils reposent sur la simulation d'un modèle probabiliste de l'évolution du système et d'un modèle de 
formation des mesures.

Chaque système simulé constitue une *particule*. On réalise en parallèle un grand nombre 
de simulations, formant un *nuage de particules*, représentant tout autant de possibilités d'évolution du système.
Les particules sont pondérées en fonction de leur vraisemblance par rapport aux mesures. 
La moyenne pondérée des particules donne  alors une estimée de l'état du système (sa position par exemple).

Ces méthodes sont couramment utilisées pour la localisation sur carte.

{% youtube aUkBa1zMKv4 640 480 %}

