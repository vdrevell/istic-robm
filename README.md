# Option ROBM

Ce dépôt héberge quelques notebooks interactifs pour l'option ROBM du Master 2 IL de l'ISTIC.
Ces derniers sont exécutables interactivement d'un simple clic grâce au service myBinder.

- [Bases de vision](http://nbviewer.jupyter.org/urls/gitlab.inria.fr/vdrevell/istic-robm/raw/master/notebooks/Vision.ipynb) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fvdrevell%2Fistic-robm/02f2463132c37ace64ef0248af38e20b4fd038b4?filepath=notebooks%2FVision.ipynb)
