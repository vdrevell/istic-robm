#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os
import urllib.parse

AUTHOR = u'Vincent Drevelle'
SITENAME = u'Option ROBM - ISTIC - M2 IL/CCN'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

#THEME = 'notmyidea'
#THEME = 'simple'
#THEME = 'themes/octopress'
#THEME = 'themes/pelican-twitchy'
THEME = 'pelican-bootstrap3'

THEME_TEMPLATES_OVERRIDES = ['templates']


#BOOTSTRAP_THEME = 'flatly'
BOOTSTRAP_THEME = 'journal'

PYGMENTS_STYLE = 'friendly'
#PYGMENTS_STYLE = 'emacs'

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

MARKUP = ('md', 'ipynb')
IGNORE_FILES = [".ipynb_checkpoints"] 

PLUGIN_PATHS = [os.path.expanduser('~/pelican-plugins')]
PLUGINS = ['liquid_tags.img', 'liquid_tags.video',
           'liquid_tags.youtube', 'liquid_tags.vimeo',
           'liquid_tags.include_code', #'liquid_tags.notebook',
           'pelican-ipynb.markup',
           'i18n_subsites',
           'filetime_from_git'] #, 'pelican-toc']

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
JINJA_FILTERS = {'rel_path': os.path.relpath,
                 'quote_plus': urllib.parse.quote_plus}
I18N_TEMPLATES_LANG = 'en'


IPYNB_USE_METACELL = True
# IPYNB_EXPORT_TEMPLATE = 'notebook.tpl'
# NOTEBOOK_DIR = '../notebooks'
IPYNB_EXTEND_STOP_SUMMARY_TAGS = [('div', ('class', 'input')), ('div', ('class', 'output')), ('h2', (None,))]
IPYNB_NB_SAVE_AS = 'notebook_files/{title}.ipynb'

# EXTRA_HEADER = open('_nb_header.html').read().decode('utf-8') if os.path.exists('_nb_header.html') else None

# PLUGINS = [ 'toc','sitemap', ]
DEFAULT_PAGINATION = False
#DEFAULT_PAGINATION = 5


TOC = {
	'TOC_HEADERS' : '^h[1-6]',
	'TOC_RUN' : 'true'
}


DISPLAY_RECENT_POSTS_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True
DISPLAY_TAGS_ON_MENU = False
EXPAND_LATEST_ON_INDEX = True
USE_FOLDER_AS_CATEGORY = True
HIDE_SIDEBAR = True

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

